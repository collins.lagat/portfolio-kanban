import { createApp } from "vue";
import "bootstrap";
import "jquery";
import "popper.js";
import { library } from "@fortawesome/fontawesome-svg-core";
import {
  faSearch,
  faArrowLeft,
  faPlusCircle,
  faPen,
  faPaperPlane,
  faPlus,
  faTrash,
  faChevronLeft,
  faChevronRight,
  faClock,
  faListUl,
  faBoxOpen,
  faEllipsisV,
  faCalendar,
} from "@fortawesome/free-solid-svg-icons";
import { FontAwesomeIcon } from "@fortawesome/vue-fontawesome";

import App from "./App.vue";
import Router from "./router";
import "./main.scss";

library.add(
  faSearch,
  faArrowLeft,
  faPlusCircle,
  faPen,
  faPaperPlane,
  faPlus,
  faTrash,
  faChevronLeft,
  faChevronRight,
  faClock,
  faListUl,
  faBoxOpen,
  faEllipsisV,
  faCalendar
);

createApp(App)
  .use(Router)
  .component("app-icon", FontAwesomeIcon)
  .mount("#app");
