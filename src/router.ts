import { createRouter, createWebHistory, RouteRecordRaw } from "vue-router";
import HomePage from "@/pages/Home.vue";
import KanbanPage from "@/pages/Kanban.vue";

const routes: RouteRecordRaw[] = [
  {
    path: "/",
    name: "boards",
    component: HomePage,
  },
  {
    path: "/kanban/",
    redirect: {
      name: "boards",
    },
  },
  {
    path: "/kanban/:boardId",
    name: "kanban",
    component: KanbanPage,
    props: true,
  },
];

export default createRouter({
  history: createWebHistory(),
  routes,
});
