import { LocalDatabase } from "../database/local-database";
import { Card } from "../database";
import { RepositoryTemplate } from "./repository-template";

export class CardRepository extends RepositoryTemplate<Card> {
  private static instance: CardRepository | null = null;
  private constructor(localDB: LocalDatabase) {
    super(localDB.getCardDao());
  }

  static getInstance(localDB: LocalDatabase): CardRepository {
    if (!this.instance) {
      this.instance = new CardRepository(localDB);
    }
    return this.instance;
  }
}
