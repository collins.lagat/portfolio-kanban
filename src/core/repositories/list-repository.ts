import { LocalDatabase } from "../database/local-database";
import { List } from "../database";
import { RepositoryTemplate } from "./repository-template";

export class ListRepository extends RepositoryTemplate<List> {
  private static instance: ListRepository | null = null;
  private constructor(localDB: LocalDatabase) {
    super(localDB.getListDao());
  }

  static getInstance(localDB: LocalDatabase): ListRepository {
    if (!this.instance) {
      this.instance = new ListRepository(localDB);
    }
    return this.instance;
  }
}
