import { LocalDatabase } from "../database/local-database";
import { Board } from "../database";
import { RepositoryTemplate } from "./repository-template";

export class BoardRepository extends RepositoryTemplate<Board> {
  private static instance: BoardRepository | null = null;
  private constructor(localDB: LocalDatabase) {
    super(localDB.getBoardDao());
  }

  static getInstance(localDB: LocalDatabase): BoardRepository {
    if (!this.instance) {
      this.instance = new BoardRepository(localDB);
    }
    return this.instance;
  }
}
