import { LocalDatabase, Todo } from "../database";
import { RepositoryTemplate } from "./repository-template";

export class TodoRepository extends RepositoryTemplate<Todo> {
  private static instance: TodoRepository | null = null;
  private constructor(localDB: LocalDatabase) {
    super(localDB.getTodoDao());
  }

  static getInstance(localDB: LocalDatabase): TodoRepository {
    if (!this.instance) {
      this.instance = new TodoRepository(localDB);
    }
    return this.instance;
  }
}
