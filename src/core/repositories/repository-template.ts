import { RepositoryMethods } from "../types/repository";
import { DataAccessObject } from "../types/dao";
import { Observable, BehaviorSubject } from "rxjs";

class SubjectWrapper<Entity> {
  private subject$: BehaviorSubject<Entity[] | undefined>;

  constructor(
    public parameters: { key?: keyof Entity; term?: string },
    initialValue: Entity[] | undefined
  ) {
    this.subject$ = new BehaviorSubject(initialValue);
  }

  asObservable(): Observable<Entity[] | undefined> {
    return this.subject$.asObservable();
  }

  next(entity: Entity[] | undefined) {
    this.subject$.next(entity);
  }
}

export abstract class RepositoryTemplate<Entity extends { id: string }>
  implements RepositoryMethods<Entity> {
  protected dao: DataAccessObject<Entity>;
  protected subjects$: SubjectWrapper<Entity>[] = [];

  constructor(DAO: DataAccessObject<Entity>) {
    this.dao = DAO;
  }

  async getItem(term: string): Promise<Entity | undefined> {
    return this.dao.read(term);
  }

  async getAllItems(
    key?: keyof Entity,
    term?: string
  ): Promise<Entity[] | undefined> {
    if (!key || !term) return this.dao.getAll();
    return this.dao
      .where(key)
      .equals(term)
      .toArray();
  }

  async watchAllItems(
    key?: keyof Entity,
    term?: string
  ): Promise<Observable<Entity[] | undefined>> {
    const items = await this.getAllItems(key, term);
    const subject$ = new SubjectWrapper({ key, term }, items);
    this.subjects$.push(subject$);
    return subject$.asObservable();
  }

  async insertItem(entity: Entity): Promise<IDBValidKey> {
    const idbKey = await this.dao.insert(entity);
    await this.notify();
    return idbKey;
  }

  async updateItem(entity: Entity): Promise<IDBValidKey> {
    const idbKey = await this.dao.update(entity);
    await this.notify();
    return idbKey;
  }

  async deleteItem(entity: Entity): Promise<void> {
    await this.dao.delete(entity.id);
    await this.notify();
  }

  protected async notify() {
    for (let index = 0; index < this.subjects$.length; index++) {
      const subject$ = this.subjects$[index];
      const items = await this.getAllItems(
        subject$.parameters.key,
        subject$.parameters.term
      );
      subject$.next(items);
    }
  }
}
