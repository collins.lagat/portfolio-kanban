import { DBSchema } from "idb";

export interface KanbanDBSchema extends DBSchema {
  board: {
    value: {
      id: string;
      name: string;
    };
    key: string;
    indexes: { name: string };
  };
  list: {
    value: {
      id: string;
      name: string;
      boardId: string;
    };
    key: string;
    indexes: { name: string; boardId: string };
  };
  card: {
    value: {
      id: string;
      name: string;
      description: string;
      listId: string;
      date: Date;
    };
    key: string;
    indexes: { name: string; listId: string };
  };
  todo: {
    value: {
      id: string;
      name: string;
      cardId: string;
      done: boolean;
    };
    key: string;
    indexes: { name: string; cardId: string };
  };
}
