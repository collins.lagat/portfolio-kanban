export enum TransactionModes {
  READONLY = "readonly",
  READWRITE = "readwrite",
  VERSIONCHANGE = "versionchange",
}
