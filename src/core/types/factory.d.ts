export interface EntityFactory<Entity, EntityRaw> {
  getEntity(entity: EntityRaw): Entity;
}
