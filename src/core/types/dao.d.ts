import { IDBPDatabase, IDB } from "idb";
import { EntityNames } from "@/core/types/entity";
import { TransactionModes } from "./dao.enum";
import { Query } from "../database/query";
import { Indexable } from "./database";

export interface DataAccessObject<Entity> {
  insert(item: Entity): Promise<IDBValidKey>;
  update(item: Entity): Promise<IDBValidKey>;
  delete(key: string): Promise<void>;
  read(key: string): Promise<Entity | undefined>;
  getAll(): Promise<Entity[] | undefined>;
  where(keyOrIndex: keyof Entity): Query<Entity>;
}
