export interface CollectionMethods<Entity> {
  addSortBy(
    compareFn: (first: Entity, last: Entity) => number
  ): CollectionMethods<Entity>;

  addFilter(predicate: () => boolean): CollectionMethods<Entity>;

  first(): Promise<Entity | undefined>;

  last(): Promise<Entity | undefined>;

  count(): Promise<number>;

  toArray(): Promise<Entity[]>;
}
