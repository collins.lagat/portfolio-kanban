import { Collection } from "@/core/database/collection";
export interface QueryMethods<Entity> {
  equals(term: string): Collection<Entity>;

  above(term: string): Collection<Entity>;

  below(term: string): Collection<Entity>;

  aboveOrEquals(term: string): Collection<Entity>;

  belowOrEquals(term: string): Collection<Entity>;

  notEqual(term: string): Collection<Entity>;
}
