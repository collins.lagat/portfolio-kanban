export type EntityNames = "board" | "list" | "card" | "todo";
export type EntityIndices = "name" | "boardId" | "listId" | "cardId";

export interface EntityMethods<RawEntity> {
  getRawObject(): RawEntity;
}

export interface RawBoard {
  id: string;
  name: string;
}

export interface RawList {
  id: string;
  name: string;
  boardId: string;
}

export interface RawCard {
  id: string;
  name: string;
  listId: string;
  description: string;
  date: Date;
}

export interface RawTodo {
  id: string;
  name: string;
  cardId: string;
  done: boolean;
}
