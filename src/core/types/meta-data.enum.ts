export enum EntityMetadataKeys {
  NAME = "entityName",
  KEYPATH = "keypath",
  INDICES = "indices",
}
export enum DatabaseMetadataKeys {
  NAME = "databaseName",
  ENTITIES = "entities",
  VERSION = "version",
}
