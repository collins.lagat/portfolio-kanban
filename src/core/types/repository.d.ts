import { Observable } from "rxjs";

export interface RepositoryMethods<Entity> {
  getItem(term: string): Promise<Entity | undefined>;
  getAllItems(key?: keyof Entity, term?: string): Promise<Entity[] | undefined>;
  watchAllItems(
    key?: keyof Entity,
    term?: string
  ): Promise<Observable<Entity[] | undefined>>;
  insertItem(entity: Entity): Promise<IDBValidKey>;
  updateItem(entity: Entity): Promise<IDBValidKey>;
  deleteItem(entity: Entity): Promise<void>;
}
