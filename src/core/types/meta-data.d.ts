import { EntityNames, EntityIndices } from "@/core/core";

export interface KeyPath {
  key: string;
  autoGenerate: boolean;
}

export interface EntityIndex {
  name: string;
  key: EntityIndices;
  unique: boolean;
}

export interface EntityMetadataValue {
  entityName: EntityNames;
  keypath: KeyPath;
  indices: EntityIndex[];
  CTor: Function;
}

export interface DatabaseMetadataValue {
  databaseName: string;
  entities: EntityMetadataValue[];
  version: number;
}
