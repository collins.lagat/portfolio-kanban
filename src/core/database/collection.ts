import { IDBPObjectStore, StoreNames } from "idb";
import { CollectionMethods } from "../types/collection";
import { KanbanDBSchema } from "../types/database";
import { EntityNames } from "../types/entity";
import { EntityFactory } from "../types/factory";

export class Collection<Entity, EntityRaw, EntityName extends EntityNames>
  implements CollectionMethods<Entity> {
  private entityList: Entity[] = [];
  private filters: (() => boolean)[] = [];
  private sorts: ((first: Entity, last: Entity) => number)[] = [];

  constructor(
    private queryKey: string,
    private queryTerm: string,
    private table: IDBPObjectStore<
      KanbanDBSchema,
      StoreNames<KanbanDBSchema>[],
      EntityName
    >,
    private entityFactory: EntityFactory<Entity, EntityRaw>
  ) {}

  addSortBy(
    compareFn: (first: Entity, last: Entity) => number
  ): Collection<Entity, EntityRaw, EntityName> {
    this.sorts.push(compareFn);
    return this;
  }

  addFilter(
    predicate: () => boolean
  ): Collection<Entity, EntityRaw, EntityName> {
    this.filters.push(predicate);
    return this;
  }

  async first(): Promise<Entity | undefined> {
    const list = await this.toArray();
    return [...list].shift();
  }

  async last(): Promise<Entity | undefined> {
    const list = await this.toArray();
    return [...list].pop();
  }

  async count(): Promise<number> {
    const list = await this.toArray();
    return list.length;
  }

  async toArray(): Promise<Entity[]> {
    const rawList = await this.performQuery();
    const list = this.buildEntity((rawList as unknown) as EntityRaw[]);
    const sortedList = this.applySort(list);
    const filteredList = this.applyFilters(sortedList);
    return filteredList;
  }

  private async performQuery(): Promise<
    Record<string, string | Date | boolean>[]
  > {
    const list: Record<string, string | Date | boolean>[] = [];
    let cursor = await this.table.openCursor();
    while (cursor) {
      const { value } = cursor;
      if (
        (value as Record<string, string | Date | boolean>)[this.queryKey] ===
        this.queryTerm
      ) {
        list.push(value);
      }
      cursor = await cursor.continue();
    }
    return list;
  }

  private buildEntity(rawEntities: EntityRaw[]): Entity[] {
    return rawEntities.map((entity) => this.entityFactory.getEntity(entity));
  }

  private applySort(arr: Entity[]): Entity[] {
    const arrCopy: Entity[] = [...arr];
    this.sorts.forEach((sortFn) => {
      arrCopy.sort(sortFn);
    });
    return arr;
  }

  private applyFilters(arr: Entity[]): Entity[] {
    let tempList = [...arr];
    this.filters.forEach((filterFn) => {
      tempList = tempList.filter(filterFn);
    });
    return arr;
  }
}
