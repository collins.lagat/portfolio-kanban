import { EntityNames } from "@/core/types/entity";
import { openDB, IDBPObjectStore, IDBPDatabase } from "idb";
import { Database } from "./decorators/database";
import { Board, List, Card, Todo } from "./entities";
import { DatabaseMetadataKeys } from "@/core/types/meta-data.enum";
import { EntityMetadataValue, EntityIndex } from "@/core/types/meta-data";
import { BoardDAO, CardDAO, ListDAO, TodoDAO } from "./daos";
import {
  BoardFactory,
  CardFactory,
  ListFactory,
  TodoFactory,
} from "./factories";
import { KanbanDBSchema } from "../types/database";

@Database("kanban-app", [Board, List, Card, Todo], 1)
export class LocalDatabase {
  static instance: LocalDatabase;

  private constructor(public idb: IDBPDatabase) {}

  static async getInstance() {
    if (!this.instance) {
      const databaseName: string = Reflect.getMetadata(
        DatabaseMetadataKeys.NAME,
        this.prototype
      );
      const entities: EntityMetadataValue[] = Reflect.getMetadata(
        DatabaseMetadataKeys.ENTITIES,
        this.prototype
      );
      const version: number = Reflect.getMetadata(
        DatabaseMetadataKeys.VERSION,
        this.prototype
      );

      if (!databaseName && !version && !!entities)
        throw new Error("Error in configuration");

      const indexedDB = await openDB(databaseName, version, {
        upgrade(db, oldVersion, newVersion) {
          console.log(`upgrading from version ${oldVersion} to ${newVersion}`);

          const createIndex = (
            index: EntityIndex,
            table: IDBPObjectStore<unknown, string[], EntityNames>
          ) => {
            table.createIndex(index.name, index.key, {
              unique: index.unique,
            });
          };

          const createEntity = (entity: EntityMetadataValue) => {
            console.log(entity);
            const table = db.createObjectStore(entity.entityName, {
              keyPath: entity.keypath.key,
              autoIncrement: entity.keypath.autoGenerate,
            });
            entity.indices.forEach((index) => createIndex(index, table));
          };

          entities.forEach(createEntity);
        },
        terminated() {
          console.log("indexedDB terminated unexpectedly");
        },
      });

      this.instance = new LocalDatabase(indexedDB);
    }
    return this.instance;
  }

  getBoardDao() {
    return new BoardDAO(
      this.idb as IDBPDatabase<KanbanDBSchema>,
      new BoardFactory()
    );
  }

  getListDao() {
    return new ListDAO(
      this.idb as IDBPDatabase<KanbanDBSchema>,
      new ListFactory()
    );
  }

  getCardDao() {
    return new CardDAO(
      this.idb as IDBPDatabase<KanbanDBSchema>,
      new CardFactory()
    );
  }

  getTodoDao() {
    return new TodoDAO(
      this.idb as IDBPDatabase<KanbanDBSchema>,
      new TodoFactory()
    );
  }
}
