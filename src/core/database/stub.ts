import { Board } from "../types/entity";

export const stub: Board[] = [
  {
    id: "1",
    name: "Board 1",
    type: "Board",
    lists: [
      {
        id: "1",
        name: "Todo",
        type: "List",
        cards: [
          {
            id: "1",
            name: "Card 1",
            type: "Card",
            description: "This is a plain description.",
            date: new Date(),
            checklist: [
              {
                id: "1",
                name: "Todo 1",
                type: "Todo",
                done: false,
              },
              {
                id: "2",
                name: "Todo 2",
                type: "Todo",
                done: false,
              },
              {
                id: "3",
                name: "Todo 3",
                type: "Todo",
                done: true,
              },
            ],
          },
        ],
      },
    ],
  },
  {
    id: "2",
    name: "Board 2",
    type: "Board",
    lists: [
      {
        id: "1",
        name: "Todo",
        type: "List",
        cards: [
          {
            id: "1",
            name: "Card 1",
            type: "Card",
            description: "This is a plain description.",
            date: new Date(),
            checklist: [
              {
                id: "1",
                name: "Todo 1",
                type: "Todo",
                done: false,
              },
              {
                id: "2",
                name: "Todo 2",
                type: "Todo",
                done: false,
              },
              {
                id: "3",
                name: "Todo 3",
                type: "Todo",
                done: true,
              },
            ],
          },
        ],
      },
    ],
  },
  {
    id: "3",
    name: "Board 3",
    type: "Board",
    lists: [
      {
        id: "1",
        name: "Todo",
        type: "List",
        cards: [
          {
            id: "1",
            name: "Card 1",
            type: "Card",
            description: "This is a plain description.",
            date: new Date(),
            checklist: [
              {
                id: "1",
                name: "Todo 1",
                type: "Todo",
                done: false,
              },
              {
                id: "2",
                name: "Todo 2",
                type: "Todo",
                done: false,
              },
              {
                id: "3",
                name: "Todo 3",
                type: "Todo",
                done: true,
              },
            ],
          },
        ],
      },
    ],
  },
  {
    id: "4",
    name: "Board 4",
    type: "Board",
    lists: [
      {
        id: "1",
        name: "Todo",
        type: "List",
        cards: [
          {
            id: "1",
            name: "Card 1",
            type: "Card",
            description: "This is a plain description.",
            date: new Date(),
            checklist: [
              {
                id: "1",
                name: "Todo 1",
                type: "Todo",
                done: false,
              },
              {
                id: "2",
                name: "Todo 2",
                type: "Todo",
                done: false,
              },
              {
                id: "3",
                name: "Todo 3",
                type: "Todo",
                done: true,
              },
            ],
          },
        ],
      },
    ],
  },
];
