import { EntityMethods, RawTodo } from "@/core/types/entity";
import { Entity, KeyPath, Index } from "../decorators/entity";

@Entity("todo")
export class Todo implements EntityMethods<RawTodo> {
  @KeyPath()
  id: string;

  @Index()
  name: string;

  @Index()
  cardId: string;

  done: boolean;

  constructor(id: string, name: string, cardId: string, done = false) {
    this.id = id;
    this.name = name;
    this.cardId = cardId;
    this.done = done;
  }
  getRawObject(): RawTodo {
    const obj: RawTodo = {
      id: this.id,
      name: this.name,
      cardId: this.cardId,
      done: this.done,
    };
    return obj;
  }
}
