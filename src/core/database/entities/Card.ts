import { EntityMethods, RawCard } from "@/core/types/entity";
import { Entity, KeyPath, Index } from "../decorators/entity";

@Entity("card")
export class Card implements EntityMethods<RawCard> {
  @KeyPath()
  id: string;

  @Index()
  name: string;

  @Index()
  listId: string;

  description: string;

  date: Date;

  constructor(
    id: string,
    name: string,
    listId: string,
    description: string,
    date: Date
  ) {
    this.id = id;
    this.name = name;
    this.listId = listId;
    this.description = description;
    this.date = date;
  }
  getRawObject(): RawCard {
    const obj: RawCard = {
      id: this.id,
      name: this.name,
      listId: this.listId,
      description: this.description,
      date: this.date,
    };
    return obj;
  }
}
