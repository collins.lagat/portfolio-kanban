import { EntityMethods, RawBoard } from "@/core/types/entity";
import { Entity, KeyPath, Index } from "../decorators/entity";

@Entity("board")
export class Board implements EntityMethods<RawBoard> {
  @KeyPath()
  id: string;

  @Index()
  name: string;

  constructor(id: string, name: string) {
    this.id = id;
    this.name = name;
  }
  getRawObject(): RawBoard {
    const obj: RawBoard = {
      id: this.id,
      name: this.name,
    };
    return obj;
  }
}
