import { EntityMethods, RawList } from "@/core/types/entity";
import { Entity, KeyPath, Index } from "../decorators/entity";

@Entity("list")
export class List implements EntityMethods<RawList> {
  @KeyPath()
  id: string;

  @Index()
  name: string;

  @Index()
  boardId: string;

  constructor(id: string, name: string, boardId: string) {
    this.id = id;
    this.name = name;
    this.boardId = boardId;
  }
  getRawObject(): RawList {
    const obj: RawList = {
      id: this.id,
      name: this.name,
      boardId: this.boardId,
    };
    return obj;
  }
}
