import { IDBPDatabase } from "idb";
import { DataAccessObject } from "@/core/types/dao";
import { Card } from "../entities";
import { KanbanDBSchema } from "@/core/types/database";
import { Query } from "../query";
import { TransactionModes } from "@/core/types/dao.enum";
import { EntityFactory } from "@/core/types/factory";
import { RawCard } from "@/core/types/entity";

export class CardDAO implements DataAccessObject<Card> {
  constructor(
    private db: IDBPDatabase<KanbanDBSchema>,
    private cardFactory: EntityFactory<Card, RawCard>,
    private entityName: "card" = "card"
  ) {}

  async getAll(): Promise<Card[] | undefined> {
    const transaction = this.db.transaction(
      this.entityName,
      TransactionModes.READONLY
    );
    const table = transaction.objectStore(this.entityName);
    const cards = await table.getAll();
    if (!cards) return;
    return cards.map((card) => this.cardFactory.getEntity(card));
  }

  insert(entity: Card): Promise<IDBValidKey> {
    const transaction = this.db.transaction(
      this.entityName,
      TransactionModes.READWRITE
    );
    const table = transaction.objectStore(this.entityName);
    return table.add(entity.getRawObject());
  }

  update(entity: Card): Promise<IDBValidKey> {
    const transaction = this.db.transaction(
      this.entityName,
      TransactionModes.READWRITE
    );
    const table = transaction.objectStore(this.entityName);
    return table.put(entity.getRawObject());
  }

  delete(key: string): Promise<void> {
    const transaction = this.db.transaction(
      this.entityName,
      TransactionModes.READWRITE
    );
    const table = transaction.objectStore(this.entityName);
    return table.delete(key);
  }

  async read(key: string): Promise<Card | undefined> {
    const transaction = this.db.transaction(
      this.entityName,
      TransactionModes.READONLY
    );
    const table = transaction.objectStore(this.entityName);
    const card = await table.get(key);
    if (!card) return;
    return this.cardFactory.getEntity(card);
  }

  where(keyOrIndex: string): Query<Card, RawCard, "card"> {
    const transaction = this.db.transaction(
      this.entityName,
      TransactionModes.READONLY
    );
    const table = transaction.objectStore(this.entityName);
    return new Query<Card, RawCard, "card">(
      keyOrIndex,
      table,
      this.cardFactory
    );
  }
}
