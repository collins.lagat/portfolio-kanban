import { IDBPDatabase } from "idb";
import { DataAccessObject } from "@/core/types/dao";
import { List } from "../entities";
import { KanbanDBSchema } from "@/core/types/database";
import { Query } from "../query";
import { TransactionModes } from "@/core/types/dao.enum";
import { EntityFactory } from "@/core/types/factory";
import { RawList } from "@/core/types/entity";

export class ListDAO implements DataAccessObject<List> {
  constructor(
    private db: IDBPDatabase<KanbanDBSchema>,
    private listFactory: EntityFactory<List, RawList>,
    private entityName: "list" = "list"
  ) {}

  async getAll(): Promise<List[] | undefined> {
    const transaction = this.db.transaction(
      this.entityName,
      TransactionModes.READONLY
    );
    const table = transaction.objectStore(this.entityName);
    const lists = await table.getAll();
    if (!lists) return;
    return lists.map((list) => this.listFactory.getEntity(list));
  }

  insert(entity: List): Promise<IDBValidKey> {
    const transaction = this.db.transaction(
      this.entityName,
      TransactionModes.READWRITE
    );
    const table = transaction.objectStore(this.entityName);
    return table.add(entity.getRawObject());
  }

  update(entity: List): Promise<IDBValidKey> {
    const transaction = this.db.transaction(
      this.entityName,
      TransactionModes.READWRITE
    );
    const table = transaction.objectStore(this.entityName);
    return table.put(entity.getRawObject());
  }

  delete(key: string): Promise<void> {
    const transaction = this.db.transaction(
      this.entityName,
      TransactionModes.READWRITE
    );
    const table = transaction.objectStore(this.entityName);
    return table.delete(key);
  }

  async read(key: string): Promise<List | undefined> {
    const transaction = this.db.transaction(
      this.entityName,
      TransactionModes.READONLY
    );
    const table = transaction.objectStore(this.entityName);
    const list = await table.get(key);
    if (!list) return;
    return this.listFactory.getEntity(list);
  }

  where(keyOrIndex: string): Query<List, RawList, "list"> {
    const transaction = this.db.transaction(
      this.entityName,
      TransactionModes.READONLY
    );
    const table = transaction.objectStore(this.entityName);
    return new Query<List, RawList, "list">(
      keyOrIndex,
      table,
      this.listFactory
    );
  }
}
