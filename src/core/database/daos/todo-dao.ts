import { IDBPDatabase } from "idb";
import { DataAccessObject } from "@/core/types/dao";
import { Todo } from "../entities";
import { KanbanDBSchema } from "@/core/types/database";
import { Query } from "../query";
import { TransactionModes } from "@/core/types/dao.enum";
import { EntityFactory } from "@/core/types/factory";
import { RawTodo } from "@/core/types/entity";

export class TodoDAO implements DataAccessObject<Todo> {
  constructor(
    private db: IDBPDatabase<KanbanDBSchema>,
    private todoFactory: EntityFactory<Todo, RawTodo>,
    private entityName: "todo" = "todo"
  ) {}

  async getAll(): Promise<Todo[] | undefined> {
    const transaction = this.db.transaction(
      this.entityName,
      TransactionModes.READONLY
    );
    const table = transaction.objectStore(this.entityName);
    const todos = await table.getAll();
    if (!todos) return;
    return todos.map((todo) => this.todoFactory.getEntity(todo));
  }

  insert(entity: Todo): Promise<IDBValidKey> {
    const transaction = this.db.transaction(
      this.entityName,
      TransactionModes.READWRITE
    );
    const table = transaction.objectStore(this.entityName);
    return table.add(entity.getRawObject());
  }

  update(entity: Todo): Promise<IDBValidKey> {
    const transaction = this.db.transaction(
      this.entityName,
      TransactionModes.READWRITE
    );
    const table = transaction.objectStore(this.entityName);
    return table.put(entity.getRawObject());
  }

  delete(key: string): Promise<void> {
    const transaction = this.db.transaction(
      this.entityName,
      TransactionModes.READWRITE
    );
    const table = transaction.objectStore(this.entityName);
    return table.delete(key);
  }

  async read(key: string): Promise<Todo | undefined> {
    const transaction = this.db.transaction(
      this.entityName,
      TransactionModes.READONLY
    );
    const table = transaction.objectStore(this.entityName);
    const todo = await table.get(key);
    if (!todo) return;
    return this.todoFactory.getEntity(todo);
  }

  where(keyOrIndex: string): Query<Todo, RawTodo, "todo"> {
    const transaction = this.db.transaction(
      this.entityName,
      TransactionModes.READONLY
    );
    const table = transaction.objectStore(this.entityName);
    return new Query<Todo, RawTodo, "todo">(
      keyOrIndex,
      table,
      this.todoFactory
    );
  }
}
