import { IDBPDatabase } from "idb";
import { DataAccessObject } from "@/core/types/dao";
import { Board } from "../entities";
import { KanbanDBSchema } from "@/core/types/database";
import { Query } from "../query";
import { TransactionModes } from "@/core/types/dao.enum";
import { EntityFactory } from "@/core/types/factory";
import { RawBoard } from "@/core/types/entity";

export class BoardDAO implements DataAccessObject<Board> {
  constructor(
    private db: IDBPDatabase<KanbanDBSchema>,
    private boardFactory: EntityFactory<Board, RawBoard>,
    private entityName: "board" = "board"
  ) {}

  async getAll(): Promise<Board[] | undefined> {
    const transaction = this.db.transaction(
      this.entityName,
      TransactionModes.READONLY
    );
    const table = transaction.objectStore(this.entityName);
    const boards = await table.getAll();
    if (!boards) return;
    return boards.map((board) => this.boardFactory.getEntity(board));
  }

  insert(entity: Board): Promise<IDBValidKey> {
    const transaction = this.db.transaction(
      this.entityName,
      TransactionModes.READWRITE
    );
    const table = transaction.objectStore(this.entityName);
    return table.add(entity.getRawObject());
  }

  update(entity: Board): Promise<IDBValidKey> {
    const transaction = this.db.transaction(
      this.entityName,
      TransactionModes.READWRITE
    );
    const table = transaction.objectStore(this.entityName);
    return table.put(entity.getRawObject());
  }

  delete(key: string): Promise<void> {
    const transaction = this.db.transaction(
      this.entityName,
      TransactionModes.READWRITE
    );
    const table = transaction.objectStore(this.entityName);
    return table.delete(key);
  }

  async read(key: string): Promise<Board | undefined> {
    const transaction = this.db.transaction(
      this.entityName,
      TransactionModes.READONLY
    );
    const table = transaction.objectStore(this.entityName);
    const board = await table.get(key);
    if (!board) return;
    return this.boardFactory.getEntity(board);
  }

  where(keyOrIndex: string): Query<Board, RawBoard, "board"> {
    const transaction = this.db.transaction(
      this.entityName,
      TransactionModes.READONLY
    );
    const table = transaction.objectStore(this.entityName);
    return new Query<Board, RawBoard, "board">(
      keyOrIndex,
      table,
      this.boardFactory
    );
  }
}
