import { IDBPObjectStore, StoreNames } from "idb";
import { KanbanDBSchema } from "../types/database";
import { EntityNames } from "../types/entity";
import { EntityFactory } from "../types/factory";
import { QueryMethods } from "../types/query";
import { Collection } from "./collection";

export class Query<Entity, EntityRaw, EntityName extends EntityNames>
  implements QueryMethods<Entity> {
  constructor(
    private queryKey: string,
    private table: IDBPObjectStore<
      KanbanDBSchema,
      StoreNames<KanbanDBSchema>[],
      EntityName
    >,
    private entityFactory: EntityFactory<Entity, EntityRaw>
  ) {}

  equals(term: string): Collection<Entity, EntityRaw, EntityName> {
    return new Collection<Entity, EntityRaw, EntityName>(
      this.queryKey,
      term,
      this.table,
      this.entityFactory
    );
  }

  above(term: string): Collection<Entity, EntityRaw, EntityName> {
    throw new Error("Please Implement.");
  }

  below(term: string): Collection<Entity, EntityRaw, EntityName> {
    throw new Error("Please Implement.");
  }

  aboveOrEquals(term: string): Collection<Entity, EntityRaw, EntityName> {
    throw new Error("Please Implement.");
  }

  belowOrEquals(term: string): Collection<Entity, EntityRaw, EntityName> {
    throw new Error("Please Implement.");
  }

  notEqual(term: string): Collection<Entity, EntityRaw, EntityName> {
    throw new Error("Please Implement.");
  }
}
