import { RawList } from "@/core/types/entity";
import { EntityFactory } from "@/core/types/factory";
import { List } from "../entities";

export class ListFactory implements EntityFactory<List, RawList> {
  getEntity(entity: RawList): List {
    return new List(entity.id, entity.name, entity.boardId);
  }
}
