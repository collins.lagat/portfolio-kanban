import { RawCard } from "@/core/types/entity";
import { EntityFactory } from "@/core/types/factory";
import { Card } from "../entities";

export class CardFactory implements EntityFactory<Card, RawCard> {
  getEntity(entity: RawCard): Card {
    return new Card(
      entity.id,
      entity.name,
      entity.listId,
      entity.description,
      entity.date
    );
  }
}
