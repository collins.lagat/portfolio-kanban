import { RawTodo } from "@/core/types/entity";
import { EntityFactory } from "@/core/types/factory";
import { Todo } from "../entities";

export class TodoFactory implements EntityFactory<Todo, RawTodo> {
  getEntity(entity: RawTodo): Todo {
    return new Todo(entity.id, entity.name, entity.cardId, entity.done);
  }
}
