import { RawBoard } from "@/core/types/entity";
import { EntityFactory } from "@/core/types/factory";
import { Board } from "../entities";

export class BoardFactory implements EntityFactory<Board, RawBoard> {
  getEntity(entity: RawBoard): Board {
    return new Board(entity.id, entity.name);
  }
}
