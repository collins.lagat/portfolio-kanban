import {
  EntityMetadataKeys,
  DatabaseMetadataKeys,
} from "@/core/types/meta-data.enum";
import {
  EntityMetadataValue,
  KeyPath,
  EntityIndex,
} from "@/core/types/meta-data";

import { EntityNames } from "@/core/types/entity";

export function Database(name: string, entities: Function[], version: number) {
  return function(Ctor: Function) {
    Reflect.defineMetadata(DatabaseMetadataKeys.NAME, name, Ctor.prototype);
    Reflect.defineMetadata(
      DatabaseMetadataKeys.VERSION,
      version,
      Ctor.prototype
    );

    const handleTable = (entityCTor: Function) => {
      const entityName: EntityNames = Reflect.getMetadata(
        EntityMetadataKeys.NAME,
        entityCTor.prototype
      );
      const keypath: KeyPath = Reflect.getMetadata(
        EntityMetadataKeys.KEYPATH,
        entityCTor.prototype
      );
      const indices: EntityIndex[] = Reflect.getMetadata(
        EntityMetadataKeys.INDICES,
        entityCTor.prototype
      );

      const _entity: EntityMetadataValue = {
        entityName,
        keypath,
        indices,
        CTor: entityCTor,
      };

      const _entities =
        Reflect.getMetadata(DatabaseMetadataKeys.ENTITIES, Ctor.prototype) ||
        [];

      Reflect.defineMetadata(
        DatabaseMetadataKeys.ENTITIES,
        [..._entities, _entity],
        Ctor.prototype
      );
    };

    entities.forEach((entity) => {
      handleTable(entity);
    });
  };
}
