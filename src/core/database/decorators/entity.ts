import { EntityIndices } from "@/core/types/entity";
import { EntityMetadataKeys } from "@/core/types/meta-data.enum";
import { KeyPath, EntityIndex } from "@/core/types/meta-data";

export function Entity(tableName: string) {
  return function(target: Function) {
    Reflect.defineMetadata(
      EntityMetadataKeys.NAME,
      tableName,
      target.prototype
    );
  };
}

export function KeyPath(autoGenerate = true) {
  return function(target: Record<"constructor", Function>, key: string) {
    const keyPath: KeyPath = {
      key,
      autoGenerate,
    };
    Reflect.defineMetadata(EntityMetadataKeys.KEYPATH, keyPath, target);
  };
}

export function Index(name = "", unique = false) {
  return function(target: Record<"constructor", Function>, key: EntityIndices) {
    name = key;
    const index: EntityIndex = {
      name,
      key,
      unique,
    };
    const indices: string[] =
      Reflect.getMetadata(EntityMetadataKeys.INDICES, target) || [];
    Reflect.defineMetadata(
      EntityMetadataKeys.INDICES,
      [...indices, index],
      target
    );
  };
}
