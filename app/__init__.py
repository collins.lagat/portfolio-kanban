from flask import Flask
from flask_restful import Api, Resource

app = Flask(__name__)
api = Api(app)

class HelloWorld (Resource):
    def get(self):
        return {"msg": "Hello World"}

api.add_resource(HelloWorld, '/')